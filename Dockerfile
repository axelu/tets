FROM centos:7

MAINTAINER Axel Urban <axel.urban.ext@hermesworld.com>

RUN echo "proxy=http://195.68.199.245:8080" >> /etc/yum.conf
#RUN sed -i s/https/http/g /etc/yum.repos.d/epel.repo
RUN sed -i s/https/http/g /etc/yum.repos.d/CentOS-Base.repo

RUN yum update && \
yum install -y wget && \
yum install -y java-1.7.0-openjdk && \
yum clean all

ENV ACTIVEMQ_VERSION 5.13.3
ENV ACTIVEMQ apache-activemq-$ACTIVEMQ_VERSION
ENV ACTIVEMQ_TCP=61616 ACTIVEMQ_AMQP=5672 ACTIVEMQ_STOMP=61613 ACTIVEMQ_MQTT=1883 ACTIVEMQ_WS=61614 ACTIVEMQ_UI=8161
ENV ACTIVEMQ_HOME /opt/activemq

RUN export http_proxy="http://195.68.199.245:8080" && \
    wget -nv http://archive.apache.org/dist/activemq/$ACTIVEMQ_VERSION/$ACTIVEMQ-bin.tar.gz && \
    mkdir -p /opt && \
    tar xf $ACTIVEMQ-bin.tar.gz -C /opt/ && \
    rm $ACTIVEMQ-bin.tar.gz && \
    ln -s /opt/$ACTIVEMQ $ACTIVEMQ_HOME && \
    useradd -r -M -d $ACTIVEMQ_HOME activemq && \
    chown 1001:1001 /opt/$ACTIVEMQ -R && \
    chmod -R 777 /opt/$ACTIVEMQ  

USER 1001
WORKDIR $ACTIVEMQ_HOME

EXPOSE $ACTIVEMQ_TCP $ACTIVEMQ_AMQP $ACTIVEMQ_STOMP $ACTIVEMQ_MQTT $ACTIVEMQ_WS $ACTIVEMQ_UI

ENV JAVA_HOME /usr/lib/jvm/jre

LABEL io.openshift.s2i.scripts-url=image:///usr/local/sti

CMD ["/bin/bash", "-c", "bin/activemq console"]

